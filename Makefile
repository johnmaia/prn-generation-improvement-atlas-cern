################################################################################
# Make file for LipMiniAnalysis
################################################################################

SHELL = /bin/sh

DEFINES = -Dextname

NVCC = nvcc
CXX  = g++
LD   = g++
ICXX  = icpc
ILD   = icpc

ROOTCFLAGS    = $(shell $(ROOTSYS)/bin/root-config --cflags)
ROOTLIBS      = $(shell $(ROOTSYS)/bin/root-config --libs)
ROOTGLIBS  = $(shell root-config --glibs) -lMinuit -lEG -lPhysics -lTreePlayer

NVCCFLAGS  = -lcurand -DMEASURE_DILEP --ptxas-options=-v -Xcompiler -pthread -Xcompiler -fopenmp -arch=sm_20 -DCUDA
CXXFLAGS   = $(ROOTCFLAGS)
ICXXFLAGS  = -O3 -openmp -pthread -DOMP -DMIC -mkl $(ROOTCFLAGS)

# specific time measurement
LIBS       = $(ROOTLIBS)
GLIBS      = $(ROOTGLIBS)

INCLUDES = -I$(incdir) -I$(ROOTSYS)/include -I$(ROOTCOREDIR)/include -I$(BOOST_ROOT)/include

################################################################################
# analysis code
################################################################################

LipMiniAnalysis = ../LipMiniAnalysis
COMMONANALYSISCODE = src/define_samples_simulation_MarkOwen.cxx src/define_samples_data_MarkOwen.cxx src/UserCommandLineOptions.cxx
LIBDIRS = -L$(LipMiniAnalysis)/lib -lLipMiniAnalysis $(LIBS) $(GLIBS)

################################################################################
# Rules
################################################################################

seq: CXXFLAGS += -DSEQ
seq: ttH_dilep_seq

omp: CXXFLAGS += -DOMP -fopenmp
omp: ttH_dilep_omp

#cuda: NVCCFLAGS += $(ROOTCFLAGS)
cuda: LIBDIRS2 = $(filter-out -pthread,$(LIBDIRS))
cuda: LIBDIRSCUDA = $(filter-out -rdynamic,$(LIBDIRS2))
cuda: CXXFLAGS += -Wno-deprecated-declarations -DCUDA
cuda: ttH_dilep_cuda

papi: CXXFLAGS += -DPAPI
papi: ttH_dilep_papi

sse: CXXFLAGS += -DSSE -mfpmath=sse -msse4.2
sse: ttH_dilep_sse

dilep_time: CXXFLAGS += -DMEASURE_DILEP
dilep_time: seq

mic: ttH_dilep_mic

default: seq
all: seq omp cuda sse

build/utilities.o: src/utilities.cxx src/utilities.h
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c src/utilities.cxx -o build/utilities.o

build/dilep_input.o: src/dilep_input.cxx src/dilep_input.h
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c src/dilep_input.cxx -o build/dilep_input.o

build/dilep_input_mic.o: src/dilep_input.cxx src/dilep_input.h
	$(ICXX) $(ICXXFLAGS) $(INCLUDES) -I/opt/intel/composer_xe_2013.1.117/mkl/include -c src/dilep_input.cxx -o build/dilep_input_mic.o

# Sequential application
build/neut_seq.o: src/seq/neut.cxx src/myvector.h src/seq/neut.h
	$(CXX) -Wall -Wextra $(CXXFLAGS) $(INCLUDES) -c src/seq/neut.cxx -o build/neut_seq.o

build/neut_mic.o: src/mic/neut.cxx src/myvector.h src/mic/neut.h
	$(ICXX) -Wall -Wextra $(ICXXFLAGS) $(INCLUDES) -I$(MKLROOT)/include -c src/mic/neut.cxx -o build/neut_mic.o

build/neut_cuda.o: src/cuda/neut.cu src/myvector.h src/cuda/neut.h
	$(NVCC) $(NVCCFLAGS) $(INCLUDES) -c src/cuda/neut.cu -o build/neut_cuda.o
#	$(CXX) $(CXXFLAGS) $(INCLUDES) -c src/cuda/neut.cxx -o build/neut_cuda.o

ttH_dilep_seq: src/ttH_dilep_seq.cxx src/ttH_dilep.h $(COMMONANALYSISCODE) build/dilep_input.o build/utilities.o build/neut_seq.o $(LipMiniAnalysis)/lib/libLipMiniAnalysis.a
	$(CXX) $(CXXFLAGS) -o bin/ttH_dilep_seq $(INCLUDES) src/ttH_dilep_seq.cxx build/neut_seq.o build/dilep_input.o build/utilities.o $(LIBDIRS)

#ttH_dilep_mic: src/ttH_dilep_mic.cxx src/ttH_dilep.h $(COMMONANALYSISCODE) build/dilep_input_mic.o build/utilities_mic.o build/neut_mic.o build/ttDKF_Best_Sol_mic.o $(LipMiniAnalysis)/libLipMiniAnalysis.a
#	$(ICXX) $(ICXXFLAGS) -o bin/ttH_dilep_mic $(INCLUDES) src/ttH_dilep_mic.cxx build/neut_mic.o build/dilep_input_mic.o build/utilities_mic.o build/ttDKF_Best_Sol_mic.o $(LIBDIRS)

ttH_dilep_mic: src/ttH_dilep_mic.cxx src/ttH_dilep.h $(COMMONANALYSISCODE) build/dilep_input.o build/neut_seq.o $(LipMiniAnalysis)/lib/libLipMiniAnalysis.a
	$(ICXX) $(ICXXFLAGS) -o bin/ttH_dilep_mic $(INCLUDES) -I$(MKLROOT)/include src/ttH_dilep_mic.cxx build/neut_seq.o build/dilep_input.o $(LIBDIRS)
# OpenMP application
#build/neut_omp.o: src/omp/neut.cxx src/myvector.h src/omp/neut.h
#	$(CXX) -Wall -Wextra $(CXXFLAGS) $(INCLUDES) -c src/omp/neut.cxx -o build/neut_omp.o

build/ttDKF_Best_Sol.o: src/ttDKF_Best_Sol.cxx src/ttDKF_Best_Sol.h
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c src/ttDKF_Best_Sol.cxx -o build/ttDKF_Best_Sol.o

build/ttDKF_Best_Sol_mic.o: src/ttDKF_Best_Sol.cxx src/ttDKF_Best_Sol.h
	$(ICXX) $(ICXXFLAGS) $(INCLUDES) -c src/ttDKF_Best_Sol.cxx -o build/ttDKF_Best_Sol_mic.o


ttH_dilep_omp: src/ttH_dilep_omp.cxx src/ttH_dilep.h $(COMMONANALYSISCODE) build/dilep_input.o build/neut_seq.o $(LipMiniAnalysis)/lib/libLipMiniAnalysis.a
	$(CXX) $(CXXFLAGS) -o bin/ttH_dilep_omp $(INCLUDES) src/ttH_dilep_omp.cxx build/neut_seq.o build/dilep_input.o $(LIBDIRS)

ttH_dilep_cuda: src/ttH_dilep_cuda.cxx src/ttH_dilep.h $(COMMONANALYSISCODE) build/dilep_input.o build/neut_seq.o $(LipMiniAnalysis)/lib/libLipMiniAnalysis.a
	$(NVCC) $(NVCCFLAGS) -std=c++11 -m64 -I/home/a51752/tools/root/6.02.10/gnu.4.9.0//include/root -o bin/ttH_dilep_cuda $(INCLUDES) src/ttH_dilep_cuda.cxx build/neut_seq.o build/dilep_input.o $(LIBDIRSCUDA)

#ttH_dilep_cuda: src/ttH_dilep_cuda.cxx src/ttH_dilep.h $(COMMONANALYSISCODE) build/dilep_input.o build/utilities.o build/neut_cuda.o build/ttDKF_Best_Sol.o $(LipMiniAnalysis)/libLipMiniAnalysis.a
#	$(NVCC) $(NVCCFLAGS) -o bin/ttH_dilep_cuda $(INCLUDES) src/ttH_dilep_cuda.cu build/neut_cuda.o build/dilep_input.o build/utilities.o build/ttDKF_Best_Sol.o $(LIBDIRSCUDA)
#	$(CXX) $(CXXFLAGS) -o bin/ttH_dilep_cuda $(INCLUDES) src/ttH_dilep_cuda.cxx build/neut_cuda.o build/dilep_input.o build/utilities.o build/ttDKF_Best_Sol.o $(LIBDIRSCUDA)

clean:
	rm -rf build/* bin/ttH_dilep*
